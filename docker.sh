docker run -d --name d-mysql \
              --hostname d-mysql \
              --restart always \
              -e MYSQL_ROOT_PASSWORD=root \
              --network macvlan0 \
              --ip=192.168.178.161 \
              --volume=d-mysql:/var/lib/mysql \
              --volume=/etc/timezone:/etc/timezone:ro\
              --volume=/etc/localtime:/etc/localtime:ro\
              mysql

docker run --name d-phpmyadmin \
                 --hostname d-phpmyadmin \
                 --link d-mysql:db  \
                 --restart always \
                 --network macvlan0 \
                 --ip=192.168.178.162 \
                 -d phpmyadmin/phpmyadmin

docker run -d --name d-iobroker \
              --link d-mysql:db \
              --restart always \
              --hostname d-iobroker \
              --volume=d-iobroker:/opt/iobroker \
              --volume=/srv/Backup/iobroker:/opt/iobroker/backups \
              --volume=/etc/timezone:/etc/timezone:ro\
              --volume=/etc/localtime:/etc/localtime:ro\
              --network macvlan0 \
              --ip=192.168.178.201  \
              -t buanet/iobroker
